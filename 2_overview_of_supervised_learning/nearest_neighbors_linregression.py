# -*- coding: utf-8 -*-
"""
Created on Sun Nov 18 16:56:08 2018

@author: koenr
"""

import csv
import numpy as np
import matplotlib.pyplot as plt

# open de files
#with open("data2.csv") as csvfile:
#    reader = csv.reader(csvfile) # change contents to floats
#    result2 = []
#    skip_first = 0
#    for row in reader:
#        if skip_first == 0:
#            print( row[0])
#            row[0] = '-1'
#            skip_first = 1
#        test = [float(i) for i in row]
#        result2.append(test)
#        
#with open("data3.csv") as csvfile:
#    reader = csv.reader(csvfile) # change contents to floats
#    result3 = []
#    skip_first = 0
#    for row in reader:
#        if skip_first == 0:
#            print( row[0])
#            row[0] = '-1'
#            skip_first = 1
#        test = [float(i) for i in row]
#        result3.append(test)

# implementatie k-nearest neighbors
# length_2 = len(result2)
# length_3 = len(result3)
#
#


# openen van de testdata

result2_test = []
result3_test = []
with open("test_data.csv") as csvfile:
    reader = csv.reader(csvfile) # change contents to floats
    result = []
    skip_first = 0
    for row in reader:

        row_split = row[0].split()
        if skip_first == 0:
            row_float = [9]
            row_float.extend([float(i) for i in row_split[1:]])
            skip_first = 1
        else:
            row_float = [float(i) for i in row_split]
            
        if row_float[0] == 2:
            result2_test.append(row_float[1:])
        if row_float[0] == 3:
            result3_test.append(row_float[1:])

result2_train = []
result3_train = []
with open("train_data.csv") as csvfile:
    reader = csv.reader(csvfile) # change contents to floats
    result = []
    skip_first = 0
    for row in reader:

        row_split = row[0].split()
        if skip_first == 0:
            row_float = [6]
            row_float.extend([float(i) for i in row_split[1:]])
            skip_first = 1
        else:
            row_float = [float(i) for i in row_split]
            
        if row_float[0] == 2:
            result2_train.append(row_float[1:])
        if row_float[0] == 3:
            result3_train.append(row_float[1:])
length2_train = len(result2_train)
length3_train = len(result3_train)
length2_test = len(result2_test)
length3_test = len(result3_test)

# =============================================================================
# misclassified training
            

teller = -1
misclassified_train_v = np.zeros(16)
for row in result2_train:
    teller = teller + 1 
    norm2 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result2_train]
    norm3 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result3_train]
    norm2.extend(norm3)
    idx = np.argsort(norm2)
    
    for k in range(16):
        classifier = sum([i < length2_train for i in idx[:k+1]])
        if classifier/(k+1) < 0.5:
            misclassified_train_v[k] = misclassified_train_v[k] + 1
         
for row in result3_train:
    teller = teller + 1 
    norm2 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result2_train]
    norm3 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result3_train]
    norm2.extend(norm3)
    idx = np.argsort(norm2)
     
    for k in range(16):
        classifier = sum([i < length2_train for i in idx[:k+1]])
        if classifier/(k+1) > 0.5:
            misclassified_train_v[k] = misclassified_train_v[k] + 1
        
misclassified_train_v = misclassified_train_v/(length2_train + length3_train)


# misclassified test 
teller = -1
misclassified_test_v = np.zeros(16)
for row in result2_test:
    teller = teller + 1 
    norm2 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result2_train]
    norm3 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result3_train]
    norm2.extend(norm3)
    idx = np.argsort(norm2)
    
    for k in range(16):
        classifier = sum([i < length2_train for i in idx[:k+1]])
        if classifier/(k+1) < 0.5:
            misclassified_test_v[k] = misclassified_test_v[k] + 1
         
for row in result3_test:
    teller = teller + 1 
    norm2 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result2_train]
    norm3 = [ np.linalg.norm(np.asarray(row) - np.asarray(row1)) for row1 in result3_train]
    norm2.extend(norm3)
    idx = np.argsort(norm2)
     
    for k in range(16):
        classifier = sum([i < length2_train for i in idx[:k+1]])
        if classifier/(k+1) > 0.5:
            misclassified_test_v[k] = misclassified_test_v[k] + 1
        
misclassified_test_v = misclassified_test_v/(length2_train + length3_train)

 # lineaire regressie: formule pg 12
 

total_list = result2_train + result3_train
X = np.array(total_list)
Y = np.append( 2*np.ones(length2_train), 3*np.ones(length3_train))
beta = np.linalg.solve(np.matmul(np.transpose(X), X), np.matmul(np.transpose(X),Y))

result_train =  sum( np.absolute( np.matmul(X,beta).round() - Y))/(length2_train+length3_train)

Y = np.append( 2*np.ones(length2_test), 3*np.ones(length3_test))
total_list = result2_test + result3_test
X = np.array(total_list)
result_test =  sum( np.absolute( np.matmul(X,beta).round() - Y))/(length2_test+length3_test)



plt.plot( range(16), misclassified_train_v, label='k-near. neighb training')
plt.plot( range(16), misclassified_test_v, label='k-near. neighb test')
plt.plot( range(16), result_train*np.ones(16), label = 'lin. regr. training')
plt.plot( range(16), result_test*np.ones(16), label = 'lin. regr. test')
plt.legend()
plt.title('percentage of misclassified')
plt.xlabel('nbr of nearest neighbours')
#plt.plot( range(16), result_train*np.ones(16), range(16), result_test*np.ones(16))
plt.show()

# X = result2
# X.append(result3)
# Y = np.append( 2*np.ones(length_2), 3*np.ones(length_3))
# 
# =============================================================================
